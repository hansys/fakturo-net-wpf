﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Projekt.Database;
using Projekt.Model;
using Projekt.View;

namespace Projekt
{
    /*
        Projekt: Evidence příjmů a výdajů pro OSVČ
        Login: HAV0277
    */

    // Main window with menu 
    public partial class MainWindow : Window
    {
        AppState appState;
        List<Button> menuButtons;
        List<Frame> pageFrames;

        public MainWindow()
        {
            InitializeComponent();
            this.appState = new AppState(
                new Db("database.db")
            );

            this.menuButtons = new List<Button>();
            this.menuButtons.Add(MenuDashboard);
            this.menuButtons.Add(MenuContacts);
            this.menuButtons.Add(MenuInvoices);
            this.menuButtons.Add(MenuCosts);
            this.menuButtons.Add(MenuUsers);
            this.menuButtons.Add(MenuLogout);

            this.pageFrames = new List<Frame>();
            this.pageFrames.Add(DashboardFrame);
            this.pageFrames.Add(ContactsFrame);
            this.pageFrames.Add(InvoicesFrame);
            this.pageFrames.Add(CostsFrame);
            this.pageFrames.Add(UsersFrame);

            this.Loaded += new RoutedEventHandler(OnLoad);
        }

        void Logout()
        {
            LoginFrame.Visibility = Visibility.Visible;
        }

        void AfterLogin(object? sender, EventArgs e)
        {
            if (this.appState.LoggedUser != null && !this.appState.LoggedUser.IsAdministrator)
            {
                MenuUsers.Visibility = Visibility.Collapsed;
            }

            LoginFrame.Visibility = Visibility.Collapsed;
            DashboardFrame.Content = new Dashboard(appState);
            ContactsFrame.Content = new Contacts(appState);
            InvoicesFrame.Content = new Invoices(appState);
            CostsFrame.Content = new Costs(appState);
            UsersFrame.Content = new Users(appState);
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            Login loginPage = new Login(appState);
            loginPage.OnLoggedIn += AfterLogin;
            LoginFrame.Content = loginPage;
        }

        void SetActiveMenu(Button senderButton)
        {
            foreach (Button button in menuButtons)
            {
                button.Style = senderButton == button ? Resources["MenuButtonActive"] as Style : Resources["MenuButton"] as Style;
            }
        }

        void SetActivePage(string pageName)
        {
            foreach (Frame frame in this.pageFrames)
            {
                frame.Visibility = frame.Tag.ToString() == pageName ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Button? senderButton = sender as Button;
            if (senderButton != null)
            {
                string tag = senderButton.Tag.ToString() ?? "";
                if (tag == "Logout")
                {
                    Logout();
                    SetActiveMenu(MenuDashboard);
                    SetActivePage("Dashboard");
                }
                else
                {
                    SetActiveMenu(senderButton);
                    SetActivePage(tag);
                }
            }  
        }

    }
}
