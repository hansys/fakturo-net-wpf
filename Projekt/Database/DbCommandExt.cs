﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt.Database
{
    public static class DbCommandExt
    {
        public static void AddParameterWithValue(this DbCommand cmd, string name, object? value)
        {
            DbParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value == null ? DBNull.Value : value;
            cmd.Parameters.Add(parameter);
        }
    }
}
