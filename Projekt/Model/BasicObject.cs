﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Projekt.Model
{
    public class BasicObject : ICloneable, INotifyPropertyChanged
    {
        [Column("Id")]
        public long? Id { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        protected void Notify([CallerMemberName] string? name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void NotifyInserted()
        {
            this.OnInserted?.Invoke(this, EventArgs.Empty);
        }

        public void NotifyUpdated()
        {
            this.OnUpdated?.Invoke(this, EventArgs.Empty);
        }

        public void NotifyDeleted()
        {
            this.OnDeleted?.Invoke(this, EventArgs.Empty);
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public event EventHandler? OnInserted;
        public event EventHandler? OnUpdated;
        public event EventHandler? OnDeleted;
    }
}
