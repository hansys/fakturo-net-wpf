﻿using Projekt.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt.Model
{
    // Object with current app state
    public class AppState
    {
        public Db Database
        {
            get;
            set;
        }

        public User? LoggedUser
        {
            get;
            set;
        }

        public Contact? LoggedUserContact
        {
            get;
            set;
        }

        public AppState(Db database)
        {
            this.Database = database;
        }

    }
}
