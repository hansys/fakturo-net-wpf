﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Projekt.Database;
using Projekt.Model;

namespace Projekt.View
{
    /// <summary>
    /// Interakční logika pro ContactEdit.xaml
    /// </summary>
    public partial class UserEditDialog : Window
    {
        AppState appState;
        User editedUser;
        User editedUserClone;
        public UserEditDialog(AppState appState, User User)
        {
            InitializeComponent();
            this.appState = appState;

            this.editedUser = User;
            this.editedUserClone = (User)User.Clone();
            this.DataContext = this.editedUserClone;

            if (this.editedUserClone.Id != null)
            {
                UsernameBox.IsEnabled = false;
                UsernameBox.Style = this.FindResource("GenericBoxInactive") as Style;
            }
            PermissionComboBox.Items.Add("Uživatel");
            PermissionComboBox.Items.Add("Administrátor");
            PermissionComboBox.SelectedIndex = this.editedUserClone.Permission;
        }


        string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, 10);
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (
                UsernameBox.Text.Length != 0 &&
                (PasswordBox.Text.Length != 0 || this.editedUserClone.Id != null)
            )
            {
                bool userContactInsertResult = true;
                if (this.editedUserClone.Id == null)
                {
                    Contact userContact = new Contact() { Type = 3 };
                    userContactInsertResult = await this.appState.Database.Insert(userContact);
                    this.editedUserClone.ContactId = userContact.Id;
                }
                if (userContactInsertResult)
                {
                    if (PasswordBox.Text.Length > 0)
                    {
                        this.editedUserClone.PasswordHash = HashPassword(PasswordBox.Text);
                    }
                    
                    foreach (PropertyInfo property in editedUser.GetType().GetProperties())
                    {
                        object? value = property.GetValue(this.editedUserClone);
                        if (property.CanWrite)
                        {
                            property.SetValue(this.editedUser, value, null);

                        }
                    }

                    bool userInsertResult = this.editedUser.Id != null ? await this.appState.Database.Update(this.editedUser) : await this.appState.Database.Insert(this.editedUser);
                    if (userInsertResult)
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Uživatel s tímto jménem již existuje");
                    }
                }
            }
            else
            {
                MessageBox.Show("Vyplňte všechna pole!");
            }
            
        }

        private void PermissionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.editedUserClone.Permission = PermissionComboBox.SelectedIndex;
        }
    }
}
