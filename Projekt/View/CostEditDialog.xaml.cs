﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Projekt.Database;
using Projekt.Model;

namespace Projekt.View
{
    public partial class CostEditDialog : Window
    {
        AppState appState;
        Cost editedCost;
        Cost editedCostClone;
        ObservableCollection<Contact> contacts;
        bool contactsLoaded;
        object lockObj = new object();

        public CostEditDialog(AppState appState, Cost cost)
        {
            InitializeComponent();
            this.appState = appState;

            this.editedCost = cost;
            this.editedCostClone = (Cost)cost.Clone();

            this.contactsLoaded = false;
            this.contacts = new ObservableCollection<Contact>();
            BindingOperations.EnableCollectionSynchronization(this.contacts, lockObj);

            this.DataContext = this.editedCostClone;
            this.Loaded += new RoutedEventHandler(OnLoad);

            SupplierComboBox.ItemsSource = this.contacts;

            TypeComboBox.Items.Add("Faktura");
            TypeComboBox.Items.Add("Účtenka");
            TypeComboBox.Items.Add("Jiný");
            TypeComboBox.SelectedIndex = this.editedCostClone.Type;
        }

        public async Task LoadContacts(ObservableCollection<Contact> collection)
        {
            var result = await this.appState.Database.SelectList<Contact>();
            foreach (var item in result)
            {
                if (item.IsSupplier && !collection.Any(collectionItem => collectionItem.Id == item.Id))
                {
                    collection.Add(item);
                }
            }
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            if (this.editedCost.SupplierId != null)
            {
                Task.Run(() =>
                    this.appState.Database.Select<Contact>(this.editedCost.SupplierId)
                ).ContinueWith(x => {
                    var contact = x.Result;
                    if (contact != null)
                    {
                        this.contacts.Add(contact);
                        SupplierComboBox.SelectedIndex = 0;
                    }
                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

            }
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (editedCostClone.CostNum == null)
            {
                var thisYearTimeStamp = Mapper.DateTimeToUnixTimestamp(
                    new DateTime(DateTime.Now.Year, 1, 1)
                ); ;
                var year = DateTime.Now.ToString("yy");
                var count = await this.appState.Database.Count<Cost>(thisYearTimeStamp, "CreatedTime") + 1 ?? 1;
                var countString = count.ToString("D4");
                editedCostClone.CostNum = $"N{year}{countString}";
            }
            foreach (PropertyInfo property in editedCost.GetType().GetProperties())
            {
                object? value = property.GetValue(this.editedCostClone);
                if (property.CanWrite)
                {
                    property.SetValue(this.editedCost, value, null);

                }
            }

            bool result = this.editedCost.Id != null ? await this.appState.Database.Update(this.editedCost) : await this.appState.Database.Insert(this.editedCost);
            if (result)
            {
                this.editedCost.Supplier = await this.appState.Database.Select<Contact>(this.editedCost.SupplierId ?? -1);
                this.Close();
            }
        }

        private void SupplierComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!contactsLoaded)
            {
                Task.Run(() =>
                    LoadContacts(this.contacts)
                ).ContinueWith((x) => {
                    this.contactsLoaded = true;
                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void SupplierComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Contact? selectedContact = SupplierComboBox.SelectedItem as Contact;
            this.editedCostClone.SupplierId = selectedContact?.Id;
        }

        private void TypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.editedCostClone.Type = TypeComboBox.SelectedIndex;
        }
    }
}
