﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using Projekt.Model;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;

namespace Projekt.View
{

    public partial class Contacts : Page
    {
        AppState appState;
        ObservableCollection<Contact> collection;
        object lockObj = new object();

        public Contacts(AppState appState)
        {
            InitializeComponent();
            this.appState = appState;

            DataContext = this.appState.LoggedUserContact;
            ListDataGrid.DataContext = this;
            this.collection = new ObservableCollection<Contact>();
            BindingOperations.EnableCollectionSynchronization(this.collection, lockObj);

            FilterComboBox.Items.Add("Vše");
            FilterComboBox.Items.Add("Dodavatelé");
            FilterComboBox.Items.Add("Odběratelé");
            FilterComboBox.SelectedIndex = 0;

            this.Loaded += new RoutedEventHandler(OnLoad);
            ListDataGrid.ItemsSource = this.collection;

        }

        public async Task Load(ObservableCollection<Contact> collection, Label count)
        {
            var result = await this.appState.Database.SelectList<Contact>(this.appState.LoggedUser?.Id, "UserId");
            foreach (var item in result)
            {
                if (!item.IsUserContact)
                {
                    collection.Add(item);
                }
            }
        }


        void OnLoad(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                Load(this.collection, Count)
            ).ContinueWith((x) => {
                Count.Content = this.collection.Count;
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }


        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Contact contact = new Contact()
            {
                UserId = this.appState.LoggedUser?.Id
            };
            contact.OnInserted += (object? sender, EventArgs args) =>
            {
                this.collection.Add(contact);
                Count.Content = this.collection.Count;
            };
            ContactEditDialog contactEdit = new ContactEditDialog(appState, contact);
            contactEdit.ShowDialog();
        }

        public void EditButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Contact? contact = button.CommandParameter as Contact;
                if (contact != null)
                {
                    ContactEditDialog contactEdit = new ContactEditDialog(appState, contact);
                    contactEdit.ShowDialog();
                }
            }
        }

        public async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Contact? obj = button.CommandParameter as Contact;
                if (obj != null && await this.appState.Database.Delete(obj))
                {
                    this.collection.Remove(obj);
                    Count.Content = this.collection.Count;
                }
            }
        }

        private void FilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox? comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                if (comboBox.SelectedIndex == 0)
                {
                    var result = this.collection;
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                if (comboBox.SelectedIndex == 1)
                {
                    var result = this.collection.Where(x => x.IsSupplier);
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                if (comboBox.SelectedIndex == 2)
                {
                    var result = this.collection.Where(x => x.IsSubscriber);
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                
            }
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "Hledat...")
            {
                searchBox.Text = "";
            }
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "")
            {
                searchBox.Text = "Hledat...";
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && e.Key == Key.Enter)
            {
                if (this.collection != null && this.collection.Count > 0 && searchBox.Text.Length > 0)
                {
                    string searchFrase = searchBox.Text.ToLower();
                    var result = this.collection.Where(
                        x => x.Name != null && x.Name.ToLower().Contains(searchFrase)
                    );
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text.Length == 0)
            {
                ListDataGrid.ItemsSource = this.collection;
                Count.Content = this.collection.Count;
            }
        }
    }
}
