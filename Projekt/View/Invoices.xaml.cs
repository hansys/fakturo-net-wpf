﻿using Projekt.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt.View
{
    public partial class Invoices : Page
    {
        AppState appState;
        ObservableCollection<Invoice> collection;
        object lockObj = new object();

        public Invoices(AppState appState)
        {
            InitializeComponent();
            this.appState = appState;

            ListDataGrid.DataContext = this;
            this.collection = new ObservableCollection<Invoice>();
            BindingOperations.EnableCollectionSynchronization(this.collection, lockObj);

            FilterComboBox.Items.Add("Vše");
            FilterComboBox.Items.Add("Nezaplacené");
            FilterComboBox.Items.Add("Zaplacené");
            FilterComboBox.SelectedIndex = 0;
            this.Loaded += new RoutedEventHandler(OnLoad);
            ListDataGrid.ItemsSource = this.collection;
        }

        public async Task Load(ObservableCollection<Invoice> collection, Label count)
        {
            var result = await this.appState.Database.SelectList<Invoice>(this.appState.LoggedUser?.Id, "UserId");
            foreach (var item in result)
            {
                item.Subscriber = await this.appState.Database.Select<Contact>(item.SubscriberId ?? -1);
                collection.Add(item);
            }
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                Load(this.collection, Count)
            ).ContinueWith((x) => {
                Count.Content = this.collection.Count;
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Invoice invoice = new Invoice()
            {
                UserId = this.appState.LoggedUser?.Id
            };
            invoice.OnInserted += (object? sender, EventArgs args) =>
            {
                this.collection.Add(invoice);
                Count.Content = this.collection.Count;
            };

            InvoiceEditDialog invoiceEdit = new InvoiceEditDialog(appState, invoice);
            invoiceEdit.ShowDialog();
        }

        public void EditButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Invoice? invoice = button.CommandParameter as Invoice;
                if (invoice != null)
                {
                    InvoiceEditDialog invoiceEdit = new InvoiceEditDialog(appState, invoice);
                    invoiceEdit.ShowDialog();
                }
            }

        }

        public async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Invoice? invoice = button.CommandParameter as Invoice;
                if (invoice != null)
                {
                    var invoiceItems = await this.appState.Database.SelectList<InvoiceItem>(invoice.Id, "InvoiceId");
                    foreach (var invoiceItem in invoiceItems)
                    {
                        await this.appState.Database.Delete(invoiceItem);
                    }
                    var result = await this.appState.Database.Delete(invoice);
                    if (result)
                    {
                        this.collection.Remove(invoice);
                        this.Count.Content = this.collection.Count;
                    }
                }
            }
        }

        private void FilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox? comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                if (comboBox.SelectedIndex == 0)
                {
                    var result = this.collection;
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                if (comboBox.SelectedIndex == 1)
                {
                    var result = this.collection.Where(x => !x.IsPaid);
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                if (comboBox.SelectedIndex == 2)
                {
                    var result = this.collection.Where(x => x.IsPaid);
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
            }
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "Hledat...")
            {
                searchBox.Text = "";
            }
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "")
            {
                searchBox.Text = "Hledat...";
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && e.Key == Key.Enter)
            {
                if (this.collection != null && this.collection.Count > 0 && searchBox.Text.Length > 0)
                {
                    string searchFrase = searchBox.Text.ToLower();
                    var result = this.collection.Where(
                        x => x.CreatedTime.Year.ToString() == searchFrase ||
                             x.InvoiceNum?.ToString() == searchFrase ||
                             (x.Note != null && x.Note.Contains(searchFrase))
                    );
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text.Length == 0)
            {
                ListDataGrid.ItemsSource = this.collection;
                Count.Content = this.collection.Count;
            }
        }

        public async void GenButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                button.Content = "Exportuji...";
                button.IsEnabled = false;

                await Task.Run(async () => {
                    InvoiceGen invoiceGen = new InvoiceGen(this.appState, this.collection);
                    await invoiceGen.Generate();
                }).ContinueWith((x) => {
                    button.Content = "Exportovat";
                    button.IsEnabled = true;
                    MessageBox.Show("Faktury byly exportovány do složky ./export/","Export dokončen");
                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

    }
}
