﻿using Projekt.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt.View
{
    public partial class Users : Page
    {
        AppState appState;
        ObservableCollection<User> collection;
        object lockObj = new object();

        public Users(AppState appState)
        {
            InitializeComponent();
            this.appState = appState;

            ListDataGrid.DataContext = this;
            this.collection = new ObservableCollection<User>();
            BindingOperations.EnableCollectionSynchronization(this.collection, lockObj);

            this.Loaded += new RoutedEventHandler(OnLoad);
            ListDataGrid.ItemsSource = this.collection;
        }

        public async Task Load(ObservableCollection<User> collection, Label count)
        {
            var result = await this.appState.Database.SelectList<User>();
            foreach (var item in result)
            {

                collection.Add(item);
            }
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                Load(this.collection, Count)
            ).ContinueWith((x) => {
                Count.Content = this.collection.Count;
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            user.OnInserted += (object? sender, EventArgs args) =>
            {
                this.collection.Add(user);
                Count.Content = this.collection.Count;
            };
            UserEditDialog userEdit = new UserEditDialog(appState, user);
            userEdit.ShowDialog();
        }

        public void EditButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                User? User = button.CommandParameter as User;
                if (User != null)
                {
                    UserEditDialog UserEdit = new UserEditDialog(appState, User);
                    UserEdit.ShowDialog();
                }
            }

        }

        public async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                User? user = button.CommandParameter as User;
                if (user != null && this.appState.LoggedUser != null && user.Id != this.appState.LoggedUser.Id)
                {
                    bool contactDeleteResult = true;
                    if (user.ContactId != null)
                    {
                        contactDeleteResult = await this.appState.Database.Delete(
                            new Contact() { Id = user.ContactId }
                        );
                    }
                   
                    bool userDeleteResult = await this.appState.Database.Delete(user);
                    if (userDeleteResult)
                    {
                        this.collection.Remove(user);
                        this.Count.Content = this.collection.Count;
                    }
                }
                else
                {
                    MessageBox.Show("Nelze smazat přihlášeného uživatele");
                }
            }
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "Hledat...")
            {
                searchBox.Text = "";
            }
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "")
            {
                searchBox.Text = "Hledat...";
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && e.Key == Key.Enter)
            {
                if (this.collection != null && this.collection.Count > 0 && searchBox.Text.Length > 0)
                {
                    string searchFrase = searchBox.Text.ToLower();
                    var result = this.collection.Where(x => x.Username != null && x.Username.ToLower().Contains(searchFrase));
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text.Length == 0)
            {
                ListDataGrid.ItemsSource = this.collection;
                Count.Content = this.collection.Count;
            }
        }
    }
}
