﻿using Projekt.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt.View
{
    public partial class Costs : Page
    {
        AppState appState;
        ObservableCollection<Cost> collection;
        object lockObj = new object();

        public Costs(AppState appState)
        {
            InitializeComponent();
            this.appState = appState;

            ListDataGrid.DataContext = this;
            this.collection = new ObservableCollection<Cost>();
            BindingOperations.EnableCollectionSynchronization(this.collection, lockObj);

            FilterComboBox.Items.Add("Vše");
            FilterComboBox.Items.Add("Faktury");
            FilterComboBox.Items.Add("Účtenky");
            FilterComboBox.Items.Add("Jiné");
            FilterComboBox.SelectedIndex = 0;
            this.Loaded += new RoutedEventHandler(OnLoad);
            ListDataGrid.ItemsSource = this.collection;
        }

        public async Task Load(ObservableCollection<Cost> collection, Label count)
        {
            var result = await this.appState.Database.SelectList<Cost>(this.appState.LoggedUser?.Id, "UserId");
            foreach (var item in result)
            {
                item.Supplier = await this.appState.Database.Select<Contact>(item.SupplierId ?? -1);
                collection.Add(item);
            }
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                Load(this.collection, Count)
            ).ContinueWith((x) => {
                Count.Content = this.collection.Count;
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Cost cost = new Cost()
            {
                UserId = this.appState.LoggedUser?.Id
            };
            cost.OnInserted += (object? sender, EventArgs args) => {
                this.collection.Add(cost);
                Count.Content = this.collection.Count;
            };
            CostEditDialog costEdit = new CostEditDialog(appState, cost);
            costEdit.ShowDialog();
        }

        public void EditButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Cost? cost = button.CommandParameter as Cost;
                if (cost != null)
                {
                    CostEditDialog costEdit = new CostEditDialog(appState, cost);
                    costEdit.ShowDialog();
                }
            }
        }

        public async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                Cost? cost = button.CommandParameter as Cost;
                if (cost != null)
                {
                    var result = await this.appState.Database.Delete(cost);
                    if (result)
                    {
                        this.collection.Remove(cost);
                        this.Count.Content = this.collection.Count;
                    }
                }
            }
        }

        private void FilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox? comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                if (comboBox.SelectedIndex == 0)
                {
                    var result = this.collection;
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
                else
                {
                    var result = this.collection.Where(x => x.Type == comboBox.SelectedIndex-1);
                    ListDataGrid.ItemsSource = result;
                    Count.Content = result.Count();
                }
            }
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "Hledat...")
            {
                searchBox.Text = "";
            }
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text == "")
            {
                searchBox.Text = "Hledat...";
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && e.Key == Key.Enter)
            {
                string searchFrase = searchBox.Text.ToLower();
                var result = this.collection.Where(x => x.CostNum != null && x.CostNum.ToLower().Contains(searchFrase));
                ListDataGrid.ItemsSource = result;
                Count.Content = result.Count();
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox? searchBox = sender as TextBox;
            if (searchBox != null && searchBox.Text.Length == 0)
            {
                ListDataGrid.ItemsSource = this.collection;
                Count.Content = this.collection.Count;
            }
        }
    }
}
