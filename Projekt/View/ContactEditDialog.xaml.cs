﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Projekt.Model;

namespace Projekt.View
{
    public partial class ContactEditDialog : Window
    {
        AppState appState;
        Contact editedContact;
        Contact editedContactClone;

        public ContactEditDialog(AppState appState, Contact contact)
        {
            InitializeComponent();
            this.appState = appState;

            this.editedContact = contact;
            this.editedContactClone = (Contact)contact.Clone();

            TypeComboBox.Items.Add("Dodavatel i odběratel");
            TypeComboBox.Items.Add("Odběratel");
            TypeComboBox.Items.Add("Dodavatel");
            if (editedContact.IsUserContact)
            {
                TypeComboBox.Items.Add("Můj kontakt");
                TypeComboBox.IsEnabled = false;
            }
            TypeComboBox.SelectedIndex = editedContact.Type;


            IsPersonalComboBox.Items.Add("Osoba");
            IsPersonalComboBox.Items.Add("Firma");
            IsPersonalComboBox.SelectedIndex = editedContact.IsPersonal ? 0 : 1;

            CountryComboBox.Items.Add("Česká republika");
            CountryComboBox.Items.Add("Slovensko");
            CountryComboBox.SelectedIndex = editedContact.CountryToIndex();
            this.DataContext = this.editedContactClone;
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.editedContactClone.SetCountryFromIndex(CountryComboBox.SelectedIndex);
            this.editedContactClone.Type = TypeComboBox.SelectedIndex;

            this.editedContact.IsPersonal = this.editedContactClone.IsPersonal;

            foreach (PropertyInfo property in editedContact.GetType().GetProperties())
            {
                object? value = property.GetValue(this.editedContactClone);
                if (property.CanWrite)
                {
                    property.SetValue(this.editedContact, value, null);

                }
            }

            bool result = this.editedContact.Id != null ? await this.appState.Database.Update(this.editedContact) : await this.appState.Database.Insert(this.editedContact);
            if (result)
            {
                this.Close();
            }
        }

        private void IsPersonalComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.editedContactClone.IsPersonal = IsPersonalComboBox.SelectedIndex == 0;
            if (IsPersonalComboBox.SelectedIndex == 0)
            {
                CompanyLabel.Visibility = CompanyBox.Visibility = Visibility.Collapsed;
                this.editedContactClone.Company = null;
                FirstNameLabel.Visibility = FirstNameBox.Visibility = LastNameLabel.Visibility = LastNameBox.Visibility = Visibility.Visible;
            }
            if (IsPersonalComboBox.SelectedIndex == 1)
            {
                CompanyLabel.Visibility = CompanyBox.Visibility = Visibility.Visible;
                this.editedContactClone.FirstName = this.editedContactClone.LastName = null;
                FirstNameLabel.Visibility = FirstNameBox.Visibility = LastNameLabel.Visibility = LastNameBox.Visibility = Visibility.Collapsed;
            }
        }

    }
}
