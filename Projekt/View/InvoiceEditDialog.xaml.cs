﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Projekt.Database;
using Projekt.Model;

namespace Projekt.View
{
    public partial class InvoiceEditDialog : Window
    {
        AppState appState;
        Invoice editedInvoice;
        Invoice editedInvoiceClone;
        ObservableCollection<InvoiceItem> invoiceItems;
        ObservableCollection<InvoiceItem> deletedInvoiceItems;
        ObservableCollection<Contact> contacts;
        bool contactsLoaded;
        object lockObj = new object();

        public InvoiceEditDialog(AppState appState, Invoice invoice)
        {
            InitializeComponent();
            this.appState = appState;

            this.editedInvoice = invoice;
            this.editedInvoiceClone = (Invoice)invoice.Clone();
            this.invoiceItems = new ObservableCollection<InvoiceItem>();
            BindingOperations.EnableCollectionSynchronization(this.invoiceItems, lockObj);

            this.contactsLoaded = false;
            this.contacts = new ObservableCollection<Contact>();
            BindingOperations.EnableCollectionSynchronization(this.contacts, lockObj);

            this.deletedInvoiceItems = new ObservableCollection<InvoiceItem>();
            InvoiceItemsListView.ItemsSource = this.invoiceItems;
            this.DataContext = this.editedInvoiceClone;
            this.Loaded += new RoutedEventHandler(OnLoad);

            SubScriberComboBox.ItemsSource = this.contacts;

            PaymentComboBox.Items.Add("Převodem");
            PaymentComboBox.Items.Add("Hotově");
            PaymentComboBox.Items.Add("Kartou");
            PaymentComboBox.SelectedIndex = this.editedInvoiceClone.PaymentType;
        }


        public async Task LoadInvoiceItems(ObservableCollection<InvoiceItem> collection, long? invoiceId)
        {
            if (invoiceId != null)
            {
                var result = await this.appState.Database.SelectList<InvoiceItem>(invoiceId, "InvoiceId");
                foreach (var item in result)
                {
                    collection.Add(item);
                }
            }
        }

        public async Task LoadContacts(ObservableCollection<Contact> collection)
        {
            var result = await this.appState.Database.SelectList<Contact>(this.appState.LoggedUser?.Id, "UserId");
            foreach (var item in result)
            {
                if (item.IsSubscriber && !collection.Any(collectionItem => collectionItem.Id == item.Id))
                {
                    collection.Add(item);
                }
            }
        }

        void OnLoad(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                LoadInvoiceItems(this.invoiceItems, this.editedInvoice.Id)
            ).ContinueWith(x => {
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

            if (this.editedInvoice.SubscriberId != null)
            {
                Task.Run(() =>
                    this.appState.Database.Select<Contact>(this.editedInvoice.SubscriberId)
                ).ContinueWith(x => {
                    var contact = x.Result;
                    if (contact != null)
                    {
                        this.contacts.Add(contact);
                        SubScriberComboBox.SelectedIndex = 0;
                    }
                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            this.invoiceItems.Add(new InvoiceItem());
        }

        private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            Button? button = sender as Button;
            if (button != null)
            {
                InvoiceItem? obj = button.CommandParameter as InvoiceItem;
                if (obj != null)
                {
                    this.deletedInvoiceItems.Add(obj);
                    this.invoiceItems.Remove(obj);
                    OnAnyChange(sender, e);
                }
            }
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (editedInvoiceClone.InvoiceNum == null)
            {
                var thisYearTimeStamp = Mapper.DateTimeToUnixTimestamp(
                    new DateTime(DateTime.Now.Year, 1, 1)
                ); ;
                var year = DateTime.Now.ToString("yy");
                var count = await this.appState.Database.Count<Invoice>(thisYearTimeStamp, "CreatedTime") + 1 ?? 1;
                var countString = count.ToString("D4");
                editedInvoiceClone.InvoiceNum = $"F{year}{countString}";
            }
            foreach (PropertyInfo property in editedInvoice.GetType().GetProperties())
            {
                object? value = property.GetValue(this.editedInvoiceClone);
                if (property.CanWrite)
                {
                    property.SetValue(this.editedInvoice, value, null);

                }
            }
            bool result = this.editedInvoice.Id != null ? await this.appState.Database.Update(this.editedInvoice) : await this.appState.Database.Insert(this.editedInvoice);

            foreach (var deletedItem in deletedInvoiceItems)
            {
                if (deletedItem.Id != null)
                {
                    var deleteResult = await this.appState.Database.Delete(deletedItem);
                }
            }

            foreach (var item in this.invoiceItems)
            {
                item.InvoiceId = this.editedInvoice.Id;
                if (item.Id != null)
                {
                    var updateResult = await this.appState.Database.Update(item);
                }
                else
                {
                    var insertResult = await this.appState.Database.Insert(item);
                }
            }

            if (result)
            {
                this.editedInvoice.Subscriber = await this.appState.Database.Select<Contact>(this.editedInvoice.SubscriberId ?? -1);
                this.Close();
            }
        }

        private void OnAnyChange(object sender, EventArgs e)
        {
            this.editedInvoiceClone.TotalPrice = this.editedInvoiceClone.TotalPriceWithoutTax = 0;
            foreach (var item in this.invoiceItems)
            {
                this.editedInvoiceClone.TotalPrice += item.TotalPrice;
                this.editedInvoiceClone.TotalPriceWithoutTax += item.TotalPriceWithoutTax;
                
            }
        }

        private void SubScriberComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!contactsLoaded)
            {
                Task.Run(() =>
                    LoadContacts(this.contacts)
                ).ContinueWith((x) => {
                    this.contactsLoaded = true;
                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void SubScriberComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Contact? selectedContact = SubScriberComboBox.SelectedItem as Contact;
            this.editedInvoiceClone.SubscriberId = selectedContact?.Id;
        }

        private void PaymentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.editedInvoiceClone.PaymentType = PaymentComboBox.SelectedIndex;
        }
    }
}
