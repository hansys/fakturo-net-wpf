﻿using Projekt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt.View
{
    public partial class Login : Page
    {
        AppState appState;

        public Login(AppState appState)
        {
            InitializeComponent();
            this.appState = appState;
            Error.Visibility = Visibility.Collapsed;
        }

        async Task<bool> VerifyUser(string username, string password)
        {
            User? user = await this.appState.Database.Select<User>(username, "Username");
            if (user != null && user.ContactId != null && BCrypt.Net.BCrypt.Verify(password, user.PasswordHash))
            {
                this.appState.LoggedUser = user;
                this.appState.LoggedUserContact = await this.appState.Database.Select<Contact>((long)user.ContactId);
                return true;
            }
            return false;
        }

        public async void SubmitButtonClick(object sender, RoutedEventArgs e)
        {
            string username = UsernameBox.Text;
            string password = PasswordBox.Text;
            bool result = await VerifyUser(username, password);
            if (!result)
            {
                Error.Visibility = Visibility.Visible;
            }
            else
            {
                UsernameBox.Text = PasswordBox.Text = "";
                Error.Visibility = Visibility.Collapsed;
                NotifyLoggedIn();
            }
        }

        public void NotifyLoggedIn()
        {
            this.OnLoggedIn?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler? OnLoggedIn;

    }
}
